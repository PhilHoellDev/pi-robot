import time
from adafruit_servokit import ServoKit

kit = ServoKit(channels=16)

while True:
	for angle in range(60,180,5):
		kit.servo[10].angle = angle
		time.sleep(1)

	break

print("Ende")
