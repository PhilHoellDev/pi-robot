
import time
from adafruit_servokit import ServoKit

kit = ServoKit(channels=16)

print("Get in inital Position")

print("Engine 1")
kit.servo[2].angle = 120
time.sleep(2)
print("Engine 2")
kit.servo[4].angle = 0
time.sleep(2)
print("Engine 3")
kit.servo[6].angle = 0
time.sleep(2)
print("Engine 4")
kit.servo[8].angle = 160
time.sleep(2)
print("Engine 5")
kit.servo[10].angle = 60
time.sleep(2)
print("Engine 6")
kit.servo[0].angle = 65
