
import time
from adafruit_servokit import ServoKit

kit = ServoKit(channels=16)

def move_to_home_pos():
	print("Get in home position")

	print("Engine 1")
	kit.servo[2].angle = 120
	time.sleep(2)
	print("Engine 2")
	kit.servo[4].angle = 0
	time.sleep(2)
	print("Engine 3")
	kit.servo[6].angle = 0
	time.sleep(2)
	print("Engine 4")
	kit.servo[8].angle = 160
	time.sleep(2)
	print("Engine 5")
	kit.servo[10].angle = 60
	time.sleep(2)
	print("Engine 6")
	# Später löschen
	for angle_m1 in range (15,65,10):
		kit.servo[0].angle = angle_m1
		time.sleep(0.5)
	#Wieder rein nehmen
	#kit.servo[0].angle = 65

	print("Home position reached")


def simple_grab():
	print("move grabber")
	kit.servo[8].angle = 50
	time.sleep(1)

	for angle_m2 in range(120,70,-10):
		print("Enginge 2 moving down")	
		kit.servo[2].angle = angle_m2
		time.sleep(0.5)

	for angle_m3 in range(0,60,10):
		print("Engine 3 moving down")
		kit.servo[4].angle = angle_m3
		time.sleep(0.5)


	for angle_m4 in range(0,80,10):
		print("Engine 4 moving down")
		kit.servo[6].angle = angle_m4
		time.sleep(0.5)


	for angle_m6 in range(65,180,20):
		print("Closing grabber")
		kit.servo[10].angle = angle_m6
		time.sleep(0.5)


	kit.servo[2].angle= 120
	time.sleep(1)
	kit.servo[4].angle = 0
	time.sleep(1)
	kit.servo[6].angle = 0

def move_right_to_left():
	time.sleep(1)
	for angle_m2 in range(120,70,-10):
		print("Engine 2 moving down")
		kit.servo[2].angle = angle_m2
		time.sleep(0.5)

	for angle_m3 in range(0,50,10):
		print("Engine 3 moving down")
		kit.servo[4].angle = angle_m3
		time.sleep(0.5)

	for angle_m4 in range(0,70,10):
		print("Engine 4 moving down")
		kit.servo[6].angle = angle_m4
		time.sleep(0.5)

	for angle_m5 in range(160,100,-10):
		print("Adjusting grabber")
		kit.servo[8].angle = angle_m5
		time.sleep(0.5)

	for angle_m1 in range(55,5,-10):
		print("Engine 1 moving right")
		kit.servo[0].angle = angle_m1
		time.sleep(0.5)

	for angle_m2 in range(70,50,-10):
		kit.servo[2].angle = angle_m2
		time.sleep(0.5)

	for angle_m6 in range(65,180,20):
		print("Close grabber")
		kit.servo[10].angle = angle_m6
		time.sleep(0.5)

	kit.servo[2].angle=120
	time.sleep(0.5)
	kit.servo[4].angle=0
	time.sleep(0.5)
	kit.servo[6].angle = 0

move_to_home_pos()

simple_grab()

move_right_to_left()


